const http = require('http');
const fs   = require('fs');
const path = require('path');
const uuid = require('uuid');

const PORT = 3000;
const allowedStatusCodes = ['100', '200', '300', '400', '500'];

http.createServer((request, response) => {
    
    if(request.method === 'GET') {
 
        let completeUrl = request.url.split('/');

        switch(completeUrl[1])
        {

            case 'uuid' : 

                response.writeHead(200, {'Content-Type' : 'application/json'});
                let uuidJsonData = {'uuid' : uuid.v4()};
                response.write(JSON.stringify(uuidJsonData));
                response.end();
            break;
            case 'status' :

                if(completeUrl[2] != undefined && allowedStatusCodes.includes(completeUrl[2])) {

                    response.writeHead(parseInt(completeUrl[2]), {'content-type' : 'text/plain'});
                    response.end(`The status code is [${String(completeUrl[2])}]`);
                } else {

                    response.writeHead(404, {'content-type' : 'text/plain'});
                    response.end(`Error Invalid Status Code`);    
                }
            break;
            case 'delay' :

                let delay = parseInt(completeUrl[2]);

                if(!delay) {
                    
                    response.writeHead(404, {'Content-Type' : 'text/plain'});
                    response.end('Error Invalid Type for delay value');
                } else {
                
                    response.writeHead(200, {'Content-Type' : 'text/plain'});
                    
                    setTimeout(() => {

                        response.end(`Timeout after ${delay} seconds`);
                    },parseInt(delay * 1000));
                }
            break;

            case 'html' :
            case 'json' :
            default : 

                let filePath = completeUrl[1] === 'html' ? './index.html' : 
                         completeUrl[1] === 'json' ? './data/data.json' : './default.html';

                let contentType = completeUrl[1] === 'json' ? 'application/json' : 'text/html';

                fs.readFile(path.join(__dirname, filePath), 'utf-8', (err, data) => {
                    if(err) {
                    
                        response.write(404, {'Content-Type' : 'text/html'});
                        response.end('ERROR File Read');
                    } else {
            
                        response.writeHead(200, {'Content-Type' : contentType});
                        response.write(data);
                        response.end();
                    }
                });
        }
    }
}).listen(PORT, () => {
    console.log(`Server listening on PORT ${PORT}`);
})
